<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

$script_path = dirname(__FILE__).'/';

$target_dir = $script_path.'../../pic/';
$target_file = $target_dir . basename($_FILES["file"]["name"]);
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
$uploadOk = 1;

if (file_exists($target_file)) {
    echo "ชื่อไฟล์ซ้ำ กรุณาเปลี่ยนชื่อไฟล์\n\r";
    $uploadOk = 0;
  }
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ) {
    echo "อนุญาตเฉพาะไฟล์ประเภท jpg,jpeg,png และ gif เท่านั้น\n\r";
    $uploadOk = 0;
  }
  
  // Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "ขออภัย อัพโหลดไม่สำเร็จ";
  // if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["file"]["tmp_name"], $target_file)) {
      //store to pic directory
      echo 'https://devops1.houseofdev.tech/Thai_Select_Ui/pic/'. basename($_FILES["file"]["name"]);

    } else {
      echo "ขออภัย อัพโหลดไม่สำเร็จ\n\r";
    }
}