<?php

    error_reporting(E_ALL);
    ini_set('display_errors', 1);

    $script_path = dirname(__FILE__).'/';
    require_once($script_path.'../../objects/object_path.php');
    require_once(STORE);

    $store_instance = new Store();
    $result_list = $store_instance->toggle_show_data($_GET['id']);
    echo $result_list;

    header('Location: ' . 'https://devops1.houseofdev.tech/Thai_Select_Ui/pages/list.php');
    //redirect back to current(all/region) listing page
    //try
    //header('Location: ' . $_SERVER['HTTP_REFERER']);


?>