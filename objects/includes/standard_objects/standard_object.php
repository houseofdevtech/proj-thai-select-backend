<?php
/**
*	Common Object
* 	Created 10-6-2012
* */

	class standard_object {
		
		protected $_config_have_arraymember;
		protected $_config_have_objectmember;
				
		function standard_object() {
			$this->check_membertype();
		}
		
		protected function check_membertype() {
			
			$this->_config_have_arraymember 	= FALSE;
			$this->_config_have_objectmember 	= FALSE;		
			
			$this_var = get_object_vars($this);
			
			foreach ($this_var as $name => $value) {
				if (is_array($this->$name) == TRUE) {
					$this->_config_have_arraymember = TRUE;
				} else 
				if (is_object($this->$name) == TRUE) {
					$this->_config_have_objectmember = TRUE;
				}
			}	
		}		
		
		function set_from_object(standard_object $object, $is_set_config_member=TRUE, $except_member_name=array()) {
			
			if (is_object($object) == FALSE) {
				return FALSE;
			}
			
			if (empty($object) == TRUE) {
				return FALSE;
			}
			
			if (get_class($this) != get_class($object)) {
				return FALSE;
			}
			
			$object_vars = get_object_vars($object);
			
			foreach ($object_vars as $name => $value) {
			
				if (is_object($object->$name) == TRUE) {
					
					$sub_object_class = get_class($object->$name);
					
					$this->$name = new $sub_object_class;
					$this->$name->set_from_object($object->$name, $is_set_config_member, $except_member_name);
				
				} else {
					
					if ($is_set_config_member == TRUE) {
						if (preg_match('/^_config_/', $name) == FALSE) {
							if (array_search($name, $except_member_name) === false) {
								$this->$name = $value;
							} 
						}
					} else {
						if (array_search($name, $except_member_name) === false) {
							$this->$name = $value;
						} 
					}
					
				}
			}
			
			return TRUE;
		} 
		
		function set_from_matchedmember_array(array $array, $is_set_config_member=TRUE, $except_member_name=array()) {
			
			if (is_array($array) == FALSE) {
				return FALSE;
			}
			
			if (empty($array) == TRUE) {
				return FALSE;
			}
			
			$this_vars = get_object_vars($this);
			
			foreach ($this_vars as $name => $value) {
			
				if (empty($array[$name]) == FALSE) {
				
					if ((is_array($array[$name]) == TRUE) && (is_object($this->$name) == TRUE)) {
					
						$this->$name->set_from_array($array[$name], $is_set_config_member, $except_member_name);
						
					} else if ((is_array($array[$name]) == TRUE) && (is_array($this->$name) == TRUE)) {
						
						$this->set_array_member($name, $array[$name] , $is_set_config_member, $except_member_name);
					
					} else {
						
						if ($is_set_config_member == TRUE) {
							if (preg_match('/^_config_/', $name) == FALSE) {
								if (array_search($name, $except_member_name) === false) {
									$this->$name = $array[$name];
								}								
							}
						} else {
							if (array_search($name, $except_member_name) === false) {
								$this->$name = $array[$name];
							}							
						}																	
					}
				}
			}
			
			return TRUE;
		}
	
		function set_from_matchedmember_object($object, $is_set_config_member=TRUE, $except_member_name=array()) {
			
			if (is_object($object) == FALSE) {
				return FALSE;
			}
			
			if (is_array($object) == TRUE) {
				return FALSE;
			}
			
			if (empty($object) == TRUE) {
				return FALSE;
			}
			
			$this_vars = get_object_vars($this);
			
			foreach ($this_vars as $name => $value) {		
			
				if (empty($object->$name) == FALSE) {
				
					if ((is_array($this->$name) == TRUE) && (is_array($object->$name) == TRUE)) {
					
						$this->set_array_member($name, $object->$name, $is_set_config_member, $except_member_name);
						
					} else if ((is_object($this->$name) == TRUE) && (is_object($object->$name) == TRUE)) {
						
						$this->$name->set_matched_member($object->$name, $is_set_config_member, $except_member_name);
						
					} else {
					
						if ($is_set_config_member == TRUE) {
							if (preg_match('/^_config_/', $name) == FALSE) {
								if (array_search($name, $except_member_name) === false) {
									$this->$name = $object->$name;
								}		
							}
						} else {
							if (array_search($name, $except_member_name) === false) {
								$this->$name = $object->$name;
							}							
						}						

					}
					
				}
				
			}	
			
			return TRUE;

		}
		
		private function set_array_member($member_name, $array_of_objects, $is_set_config_member=TRUE, $except_member_name=array()) {
			$member_type = '_config_'.$member_name.'_array_type';
			
			$array_count=0;
			foreach ($array_of_objects as $name => $value) {
				
				$member_array_name = $member_name.'['.$array_count.']';
				eval('$this->'.$member_array_name.' = new $this->$member_type;');
				
				if (is_array($array_of_objects[$array_count]) == TRUE) {

					eval('$this->'.$member_array_name.'->set_from_matchedmember_array($array_of_objects[$array_count], $is_set_config_member, $except_member_name);');
					
				} else {

					eval('$this->'.$member_array_name.'->set_from_matchedmember_object($array_of_objects[$array_count], $is_set_config_member, $except_member_name);');

				}
				
				$array_count++;
			}
			
			return TRUE;
		}
		
		function reorder_arraymember($member_name='') {
		
			if ($member_name == '') {
				return FALSE;
			}
			
			if (is_array($this->$member_name) == FALSE) {
				return FALSE;
			}
			
			$this->$member_name = array_reverse(array_reverse($this->$member_name));
		}
		
		function remove_arraymember_element($member_name='', $element_index=FALSE) {
			
			if ($member_name == '') {
				return FALSE;
			}
			
			if ($element_index === FALSE) {
				return FALSE;
			}
			
			if (is_array($this->$member_name) == FALSE) {
				return FALSE;
			}
			
			if ((is_numeric($element_index) == FALSE) || ($element_index < 0)) {
				return FALSE;
			}
			
			unset($this->$member_name[$element_index]);
			$this->$member_name = array_reverse(array_reverse($this->$member_name));
		}
		
		function set_from_serialize($serialized_string) {
			
			$object = unserialize($serialized_string);
			
			$this->set_from_object($object);
		}
		
		function set_from_url_serialize($url_serialized_string) {
		
			$this->set_from_serialize(urldecode($url_serialized_string));
			
		}
		
		function set_from_json($jsonencoded_string) {
			
			$object = json_decode($jsonencoded_string);
			
			$this->set_from_matchedmember_object($object);
		}
		
		function set_from_url_json($url_jsonencoded_string) {
		
			$this->set_from_json(urldecode($url_jsonencoded_string));
		
		}
		
		function get_serialize() {
		
			return serialize($this);
			
		}
		
		function get_url_serialize() {
			
			return urlencode($this->get_serialize());
			
		}
		
		function get_json() {
			
			return json_encode($this);
			
		}
		
		function get_url_json() {
			
			return urlencode($this->get_json());
			
		}
	}
	
?>