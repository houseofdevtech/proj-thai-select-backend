<?php
/**
 * This section is for serving request from remote bridge object
 *
 * TO USE
 * DEFINE('BRIDGE_SERVER_FILENAME', basename(__FILE__));
 * require_once this file at the bottom of the object which decided to be served remote bridge request
 *
 * Created by Yim 28-1-2014
 */

if (empty($_POST['bridge_msg']) == false) {

	$request = $_POST['bridge_msg'];

	$mcrypt_obj = new Anti_Mcrypt(BRIDGE_SERVER_FILENAME);
	$decrypted_request = $mcrypt_obj->decrypt($request);

	if (empty($decrypted_request) == true) {
		echo $mcrypt_obj->encrypt('ERROR_DECRYPT_REQUEST_MSG');
		exit;
	}

	$request_msg_object = unserialize($decrypted_request);

	$remote_object = unserialize($request_msg_object->object);

	if (empty($remote_object) == true) {
		echo $mcrypt_obj->encrypt('ERROR_EMPTY_REQUEST_OBJECT');
		exit;
	}
	if (is_subclass_of($remote_object, 'standard_object') == FALSE) {
		echo $mcrypt_obj->encrypt('ERROR_INVALID_REQUEST_OBJECT');
		exit;
	}
	if ((empty($request_msg_object->command) == true) || (is_array($request_msg_object->command) == false)) {
		echo $mcrypt_obj->encrypt('ERROR_EMPTY_REQUEST_COMMAND');
		exit;
	}
	
	$remote_object_type = get_class($remote_object);
	$request_object = new $remote_object_type;
	unset($remote_object_type);
	
	$request_object->set_from_object($remote_object, FALSE);

	$object_command = '';
	$object_command_result = '';
	foreach ($request_msg_object->command as $index => $command) {
		$object_command .= '
					$object_command_result = $request_object->'.$command.';
			';
	}

	ob_start();
	eval($object_command);
	unset($object_command);
	$eval_display = ob_get_clean();
	if (empty($eval_display) == false) {
		echo $mcrypt_obj->encrypt('NOTICE_COMMAND_DISPLAY '.$eval_display);
		exit;
	}
	unset($eval_display);

	$remote_object->set_from_object($request_object, FALSE);
	unset($request_object);
	
	$response_msg_object = new stdClass();
	$response_msg_object->result = $object_command_result;
	$response_msg_object->object = serialize($remote_object);

	echo $mcrypt_obj->encrypt(serialize($response_msg_object));
	unset($response_msg_object);
	unset($remote_object);
	unset($object_command_result);
}

?>